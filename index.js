// bai 1
/**đầu vào: nhập số ngày làm
 * 
 * xử lý
 * tạo số ngày làm
 * tạo tiền lương 1 ngày
 * tính tiền lương = lương 1 ngày * số ngày
 * 
 * đầu ra : tiền lương
**/
var luong1Ngay, soNgayLam, tongLuong;
luong1Ngay = 100000;
soNgayLam = 4;
tongLuong = luong1Ngay * soNgayLam;
console.log("🚀 ~ file: index.js:6 ~ tongLuong:", tongLuong)

// bai 2
/**
 * Đầu vào: tạo 5 biến và biến trungbinh sau đó nhập 5 số thực
 * 
 * xử lý
 * nhập giá trị vào các biến số thực(so1-so5)
 * tính giá trị trung bình của 5 số
 * 
 * đầu ra: giá trị trung bình 5 số (trungbinh)
 */
var so1, so2, so3, so4, so5, trungbinh;
so1 = 5;
so2 = 10;
so3 = 15;
so4 = 20;
so5 = 25;
trungbinh = (so1 + so2 + so3 + so4 + so5) / 5;
console.log("🚀 ~ file: index.js:34 ~ trungbinh:", trungbinh)

// bai 3
/**
 * Đầu vào: nhập số tiền usd
 * 
 * xử lý
 * tạo biến usd với vnd và chuyendoi
 * nhập giá trị cho vnd
 *
 * đầu ra: chuyendoi = usd * VND
 */
var vnd, usd, chuyendoi;
usd = 5;
vnd = 23500;
chuyendoi = usd * vnd;
console.log("🚀 ~ file: index.js:50 ~ chuyendoi:", chuyendoi)

// bai 4
/**
 * Đầu vào: nhập chiều dài và chiều rộng
 * 
 * xử lý
 * tạo biến chieuDai với chieuRong và dienTich,chuVi
 * 
 * đầu ra: Diện tích= chieuDai*chieuRong
 *         Chu Vi= (chieuDai+chieuRong)*2
 */
var chieuDai, chieuRong, chuVi, dienTich;
chieuDai = 10;
chieuRong = 5;
chuVi = (chieuDai + chieuRong) * 2;
console.log("🚀 ~ file: index.js:66 ~ chuVi:", chuVi)
dienTich = chieuDai * chieuRong;
console.log("🚀 ~ file: index.js:68 ~ dienTich:", dienTich)

// bai 5
/**
 * Đầu vào: nhập vào một số có 2 ký số vd(12,44,83)
 * 
 * xử lý
 * tạo biến soVuaNhap,soHangDonVi với soHangChuc và tongCacSo
 * lấy giá trị hàng đơn vị = soVuaNhap%10
 * lấy giá trị hàng chục = soVuaNhap/10
    **nên dùng thêm hàm Math.Floor để làm tròn
    Đầu ra: tổng các số (tongCacSo) = soHangDonVi + soHangChuc
*/
var soVuaNhap, soHangDonVi, soHangChuc, tongCacSo;
soVuaNhap = 29;
soHangDonVi = soVuaNhap % 10;
soHangChuc = soVuaNhap / 10;
tongCacSo = Math.floor(soHangDonVi + soHangChuc);
console.log("🚀 ~ file: index.js:86 ~ tongCacSo:", tongCacSo)
